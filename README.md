# Revolt Theme

## Contents of this file

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

Revolt is a Drupal 9/10 theme, designed to work out of the box or as a customizable base theme for creating flexible subthemes. This versatile theme is ready to use as-is or can be easily customized for developers, allowing you to create highly flexible web designs with minimal effort.

Built by Virasat Solutions specifically for the Drupal community, Revolt is optimized for performance and ease of use. It prioritizes lean, efficient code to ensure a fast website launch while providing an elegant, customizable framework for developers.

Key Features:
- Mobile-first responsive approach
- Component-based design
- Pre-built mobile menu
- Prioritizes semantic HTML with mixins and utility SCSS
- SASS inheritance workflow which is easily overridable
- Responsive Display Suite styling for common layouts
- Theme update hooks support for complex upgrade paths
- Sticky header (header remains fixed at the top while scrolling)
- Configurable hero banner with content position options
- Configurable header with width and background class options
- Attractive form and form fields

For a full description of the theme, visit the [project page](https://www.drupal.org/project/revolt).  
Use the [Issue queue](https://www.drupal.org/project/issues/revolt) to submit bug reports, feature suggestions, or track changes.

## Requirements

This theme requires Drupal core >= 9.0.

**Note**: While Revolt works out of the box, it is highly recommended to have a basic understanding of SASS and component-based design for advanced customizations.

## Installation

To install Revolt:

1. Install the theme as you would any contributed Drupal theme.
2. Visit the [project page](https://www.drupal.org/project/revolt) or the Drupal documentation for further installation details.

### Set Revolt as the default theme

1. Navigate to Admin > Appearance.
2. Install the Revolt theme by clicking "Install" under Revolt.
3. At the bottom of the page, set Revolt as your default theme.

## Configuration

Revolt comes with several configuration options that allow you to customize the theme's appearance and functionality without modifying code. 

### Hero Banner Configuration
- Navigate to Admin > Appearance > Settings > Revolt to configure the hero banner.
- Options include content positioning and display settings for optimal visual impact.

### Header Configuration
- Customize the header with width adjustments and background class options directly from the theme settings.

### Sticky Header
- Enable or disable the sticky header feature (a header that stays fixed at the top of the page while scrolling) via the theme settings.

For more advanced customization, you can easily create a subtheme and override the provided settings and styles.

## Maintainers

Current maintainers:
- Yogesh Kumar ([@yogeshk](https://www.drupal.org/u/yogeshk))
- Abhiyanshu Rawat ([@abhiyanshu](https://www.drupal.org/u/abhiyanshu))

If you would like to contribute, please visit the [Issue queue](https://www.drupal.org/project/issues/revolt) to report issues or submit patches.
